var express = require('express');

var app = express();


// vendor library
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var mysql = require("mysql");


app.use(express.static(__dirname + '/public'));


// custom library
// model
var Model = require('./model');

// index
var index = function(req, res, next) {
   if(!req.isAuthenticated()) {
      res.redirect('/signin');
   } else {

      var user = req.user;

      if(user !== undefined) {
         user = user.toJSON();
      }
      res.render('index', {title: 'Home', user: user});
   }
};


// create claim
var claimMgr = function(req, res, next){
   res.render( 'claims',{title: 'createclaim'} );
}

// sign in
// GET
var signIn = function(req, res, next) {
   if(req.isAuthenticated()) res.redirect('/');
   res.render('signin', {title: 'Sign In'});
};



var pool      =    mysql.createPool({
   connectionLimit : 100, //important
   host     : 'localhost',
   user     : 'root',
   password : 'root',
   database : 'sakila',
   debug    :  false
});


function handle_database(req,res, next) {


   pool.getConnection(function(err,connection) {
      if (err) {
         connection.release();
         res.json({"code": 100, "status": "Error in connection database"});
         return;
      }

      console.log('connected as id ' + connection.threadId + " " + req.url);

      if (req.url == '/process_post') {
         console.log('Last insert ID:', req.body);

         var employee = { driver_fname: req.body.first_name, driver_initials: req.body.initial_name, driver_lname: req.body.last_name,
                        audit_insert_user:req.body.reportedby,job_position:req.body.jobtitle,
                     dob:req.body.dob, age:req.body.age, driver_ssn:req.body.ssn, job_position:req.body.jobtitle, audit_insert_app:'NodeJS_APP',
            driver_empid: req.body.employeeid, driver_relation: req.body.relation, driver_license: req.body.driverLicense,
            home_phone_num:req.body.contactHomeNo, cell_phone_num:req.body.contactCellNo, work_phone_num:req.body.contactWorkNo, driver_email_id:req.body.emailId,
            vin:req.body.vin, _year:req.body.year };
         connection.query('INSERT INTO claims SET ?', employee, function (err, callback) {
            connection.release();
            if (!err) {
               console.log('Last insert ID:', callback.insertId);
               res.render( 'index', { title: 'Claim Created', user: req.body.first_name + req.body.last_name, claim_id: callback.insertId} );

               //res.json(JSON.stringify(callback.insertId));
            }
            if (err) throw err;

         });
      }
      if (req.query.search == 'search') {
         console.log('claim Id during search' + req.query.q);
         if(req.query.q == null)
         {
            res.render( 'index', { title: 'Claims Info', user: req.query.q, claim_found:null} );
         }
         connection.query('SELECT * FROM claims where claims_id =?',req.query.q, function (err, rows) {
            connection.release();
            if (err) throw err;
            console.log('Data received from Db:\n' + rows);
            if(rows.length >0)
            {
               res.render( 'index', { title: 'Claims Info', user: rows[0].driver_fname, claim_found:rows} );
            }
            else
               res.render( 'index', { title: 'Claims Info', user: req.query.q, claim_found:null} );
            //renderJSON.stringify(rows);
            //res.json(rows)
         });
      }

      connection.on('error', function(err) {
         res.json({"code" : 100, "status" : "Error in connection database"});
         return;
      });
   });
}

var processPost = function(req, res, next) {
   handle_database(req,res, next);

}


var search = function(req, res, next) {
   handle_database(req,res, next);

}


// sign in
// POST
var signInPost = function(req, res, next) {
   passport.authenticate('local', { successRedirect: '/',
                          failureRedirect: '/signin'}, function(err, user, info) {
      if(err) {
         return res.render('signin', {title: 'Sign In', errorMessage: err.message});
      } 

      if(!user) {
         return res.render('signin', {title: 'Sign In', errorMessage: info.message});
      }
      return req.logIn(user, function(err) {
         if(err) {
            return res.render('signin', {title: 'Sign In', errorMessage: err.message});
         } else {
            return res.redirect('/');
         }
      });
   })(req, res, next);
};

// sign up
// GET
var signUp = function(req, res, next) {
   if(req.isAuthenticated()) {
      res.redirect('/');
   } else {
      res.render('signup', {title: 'Sign Up'});
   }
};

// sign up
// POST
var signUpPost = function(req, res, next) {
   var user = req.body;
   var usernamePromise = null;
   usernamePromise = new Model.User({username: user.username}).fetch();

   return usernamePromise.then(function(model) {
      if(model) {
         res.render('signup', {title: 'signup', errorMessage: 'username already exists'});
      } else {
         //****************************************************//
         // MORE VALIDATION GOES HERE(E.G. PASSWORD VALIDATION)
         //****************************************************//
         var password = user.password;
         var hash = bcrypt.hashSync(password);

         var signUpUser = new Model.User({username: user.username, password: hash});

         signUpUser.save().then(function(model) {
            // sign in the newly registered user
            signInPost(req, res, next);
         });	
      }
   });
};

// sign out
var signOut = function(req, res, next) {
   if(!req.isAuthenticated()) {
      notFound404(req, res, next);
   } else {
      req.logout();
      res.redirect('/signin');
   }
};

// 404 not found
var notFound404 = function(req, res, next) {
   res.status(404);
   res.render('404', {title: '404 Not Found'});
};

// export functions
/**************************************/
// index
module.exports.index = index;

// sigin in
// GET
module.exports.signIn = signIn;
// POST
module.exports.signInPost = signInPost;

// sign up
// GET
module.exports.signUp = signUp;
// POST
module.exports.signUpPost = signUpPost;

// sign out
module.exports.signOut = signOut;

// 404 not found
module.exports.notFound404 = notFound404;

// claim Mgr
module.exports.claimMgr = claimMgr;

module.exports.processPost = processPost;


module.exports.search = search;