/**
 * Created by jayt on 11/28/2015.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require("mysql");
var flash = require('connect-flash');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bcrypt = require('bcrypt-nodejs');
var ejs = require('ejs');
var path = require('path');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })


// custom libraries
// routes
var route = require('./route');
// model
var Model = require('./model');

app.set('port', process.env.PORT || 8081);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(bodyParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('public'));
app.use(cookieParser('secret'));
app.use(session({cookie: {secret: 'secret strategic xxzzz code'}}));
app.use(flash());
//app.use(express.static(__dirname + '/public'));


passport.use(new LocalStrategy(function(username, password, done) {
    new Model.User({username: username}).fetch().then(function(data) {
        var user = data;
        if(user === null) {
            return done(null, false, {message: 'Invalid username or password'});
        } else {
            user = data.toJSON();
            if(!bcrypt.compareSync(password, user.password)) {
                return done(null, false, {message: 'Invalid username or password'});
            } else {
                return done(null, user);
            }
        }
    });
}));

passport.serializeUser(function(user, done) {
    done(null, user.username);
});

passport.deserializeUser(function(username, done) {
    new Model.User({username: username}).fetch().then(function(user) {
        done(null, user);
    });
});



var pool      =    mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'sakila',
    debug    :  false
});


function handle_database(req,res) {


    pool.getConnection(function(err,connection) {
        if (err) {
            connection.release();
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }

        console.log('connected as id ' + connection.threadId + " " + req.url);

        if (req.url == '/process_post') {
            console.log('Last insert ID:', req.body);

            var employee = { driver_fname: req.body.first_name, job_position:req.body.jobtitle, driver_lname: req.body.last_name, audit_insert_user:req.body.reportedby, age:req.body.age, driver_ssn:req.body.ssn, audit_insert_app:'NodeJS_APP', vin:req.body.vin, _year:req.body.year };
            connection.query('INSERT INTO claims SET ?', employee, function (err, callback) {

                connection.release();
                if (!err) {
                    console.log('Last insert ID:', callback.insertId);
                    //res.render( 'index.htm', { claim_id:callback.insertId} );
                    //res.json(JSON.stringify(callback.insertId));
                    res.redirect('index.htm');
                    /*if(callback.insertId!=null) {
                        res.locals.success_messages = req.flash('Success');
                    } else {
                        res.locals.error_messages = req.flash('error_messages');
                    }*/
                }
                if (err) throw err;

            });
        }
        if (req.query.search == 'search') {
            console.log('claim Id during search' + req.query.q);
            connection.query('SELECT * FROM claims where claims_id =?',req.query.q, function (err, rows) {
                connection.release();
                if (err) throw err;
                console.log('Data received from Db:\n' + rows);
                res.json(rows)
            });
        }

        connection.on('error', function(err) {
            res.json({"code" : 100, "status" : "Error in connection database"});
            return;
        });
    });
}



// GET
app.get('/', route.index);

// signin
// GET
app.get('/signin', route.signIn);
// POST
app.post('/signin', route.signInPost);

// signup
// GET
app.get('/signup', route.signUp);
// POST
app.post('/signup', route.signUpPost);

// logout
// GET
app.get('/signout', route.signOut);

/********************************/

/********************************/
// 404 not found
app.use(route.notFound404);


app.get("/search",function(req,res){
    handle_database(req,res);
});

app.get('/claimMgr', function (req, res) {
    res.sendFile( __dirname + "/" + "ClaimMgr.htm" );
})


app.get('/index.htm', function (req, res) {
    res.sendFile( __dirname + "/" + "index.htm" );
})

app.get('/tabcontent.css', function (req, res) {
    res.sendFile( __dirname + "/" + "tabcontent.css" );
})

app.get('/tabcontent.js', function (req, res) {
    res.sendFile( __dirname + "/" + "tabcontent.js" );
})


app.post('/process_post', urlencodedParser, function (req, res) {
    handle_database(req,res);

})


/*
app.all('/', function(req, res){
    req.flash('test', 'it worked');
    res.redirect('index.htm');
});
*/

app.all('/test', function(req, res){
    res.send(JSON.stringify(req.flash('test')));
});

var server = app.listen(app.get('port'), function () {

    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)

})